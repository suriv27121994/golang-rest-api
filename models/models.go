package models

import (
	"database/sql"
	"fmt"
	"golang-rest-api/config"
	"log"

	_ "github.com/lib/pq" // postgres golang driver
)

// Book schema of the Books table
// we try if the data is null
// if the return data is null, please use NullString, for example below
// Author config.NullString `json:"author"`
type Book struct {
	ID            int64  `json:"id"`
	Judul_buku    string `json:"judul_buku"`
	Penulis       string `json:"penulis"`
	Tgl_publikasi string `json:"tgl_publikasi"`
}

func AddBook(book Book) int64 {

	// connect to postgres db
	db := config.CreateConnection()

	// we close the connection at the end of the process
	defer db.Close()

	// we create an insert query
// returning the value of id will return the id of the book inserted into the db
	sqlStatement := `INSERT INTO book (judul_buku, penulis, tgl_publikasi) VALUES ($1, $2, $3) RETURNING id`

	// the entered id will be stored in this id
	var id int64

	// Scan function will save insert id inside id
	err := db.QueryRow(sqlStatement, book.Judul_buku, book.Penulis, book.Tgl_publikasi).Scan(&id)

	if err != nil {
		log.Fatalf("Cannot execute query. %v", err)
	}

	fmt.Printf("Insert data single record %v", id)

	// return insert id
	return id
}

// Get one book
func GetAllBook() ([]Book, error) {
	// connect to postgres db
	db := config.CreateConnection()

	// we close the connection at the end of the process
	defer db.Close()

	var books []Book

	// we create a select query
	sqlStatement := `SELECT * FROM book`

	// execute sql query
	rows, err := db.Query(sqlStatement)

	if err != nil {
		log.Fatalf("Cannot execute query. %v", err)
	}

	// We close the execution of the sql query process
	defer rows.Close()

	// We iterate to retrieve the data
	for rows.Next() {
		var book Book

		// We take the data and unmarshal it into the struct
		err = rows.Scan(&book.ID, &book.Judul_buku, &book.Penulis, &book.Tgl_publikasi)

		if err != nil {
			log.Fatalf("Cannot retrieve data. %v", err)
		}

		// Put it in a slice of books
		books = append(books, book)

	}

	// return empty book or if error
	return books, err
}

// Get one book
func GetOneBook(id int64) (Book, error) {
	// connwct to db postgres
	db := config.CreateConnection()

	// We close connection in last process
	defer db.Close()

	var book Book

	// Create sql query
	sqlStatement := `SELECT * FROM book WHERE id=$1`

	// Execution sql statement
	row := db.QueryRow(sqlStatement, id)

	err := row.Scan(&book.ID, &book.Judul_buku, &book.Penulis, &book.Tgl_publikasi)

	switch err {
	case sql.ErrNoRows:
		fmt.Println("No data searched!")
		return book, nil
	case nil:
		return book, nil
	default:
		log.Fatalf("Can not retrieve data. %v", err)
	}

	return book, err
}

// Update the user in the DB
func UpdateBook(id int64, book Book) int64 {

	// Connect to db postgres
	db := config.CreateConnection()

	// We close connection in last process
	defer db.Close()

	// We update sql query 
	sqlStatement := `UPDATE book SET judul_buku=$2, penulis=$3, tgl_publikasi=$4 WHERE id=$1`

	// Execution sql statement
	res, err := db.Exec(sqlStatement, id, book.Judul_buku, book.Penulis, book.Tgl_publikasi)

	if err != nil {
		log.Fatalf("Can not execute query. %v", err)
	}

	// Check how many rows/data are updated
	rowsAffected, err := res.RowsAffected()

	// We check
	if err != nil {
		log.Fatalf("Error when checking updated rows/data. %v", err)
	}

	fmt.Printf("Total rows/records updated %v\n", rowsAffected)

	return rowsAffected
}

func DeleteBook(id int64) int64 {

	// Connect to db postgres
	db := config.CreateConnection()

	// We close connect in last process
	defer db.Close()

	// Make sql query
	sqlStatement := `DELETE FROM book WHERE id=$1`

	// Execution sql statement
	res, err := db.Exec(sqlStatement, id)

	if err != nil {
		log.Fatalf("Can not execute query. %v", err)
	}

	// Check how many data/rows were deleted
	rowsAffected, err := res.RowsAffected()

	if err != nil {
		log.Fatalf("Can't find data. %v", err)
	}

	fmt.Printf("Total data deleted %v", rowsAffected)

	return rowsAffected
}