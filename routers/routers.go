package router

import (
	"golang-rest-api/controllers"

	"github.com/gorilla/mux"
)

func Router() *mux.Router {

	router := mux.NewRouter()

	router.HandleFunc("/api/book", controllers.GetAllBook).Methods("GET", "OPTIONS")
	router.HandleFunc("/api/book/{id}", controllers.GetBook).Methods("GET", "OPTIONS")
	router.HandleFunc("/api/book", controllers.AddBook).Methods("POST", "OPTIONS")
	router.HandleFunc("/api/book/{id}", controllers.UpdateBook).Methods("PUT", "OPTIONS")
	router.HandleFunc("/api/book/{id}", controllers.DeleteBook).Methods("DELETE", "OPTIONS")

	return router

}