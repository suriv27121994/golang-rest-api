package controllers

import (
	"encoding/json" // The package for the encode and the decode json chnage to struct dan sebaliknya
	"fmt"
	"strconv" // package used to convert strings to int type

	"log"
	"net/http" // used to access request and response objects from APIs

	"golang-rest-api/models" //models package where Books is defined

	"github.com/gorilla/mux" // used to get parameters from the router
	_ "github.com/lib/pq"    // postgres golang driver
)

type response struct {
	ID      int64  `json:"id,omitempty"`
	Message string `json:"message,omitempty"`
}

type Response struct {
	Status  int           `json:"status"`
	Message string        `json:"message"`
	Data    []models.Book `json:"data"`
}

// AddBook
func AddBook(w http.ResponseWriter, r *http.Request) {

	// create an empty user of type models.User
	// we create an empty book with the model type. Book
	var book models.Book

	// Decode json request data to the book
	err := json.NewDecoder(r.Body).Decode(&book)

	if err != nil {
		log.Fatalf("Cannot decode from request body.  %v", err)
	}

	// call the models and then insert the book
	insertID := models.AddBook(book)

	// format response object
	res := response{
		ID:      insertID,
		Message: "Book data has been added",
	}

	// Send response
	json.NewEncoder(w).Encode(res)
}

// RetrieveBook retrieves single data with id parameter
func GetBook(w http.ResponseWriter, r *http.Request) {
	// We set the header
	w.Header().Set("Context-Type", "application/x-www-form-urlencoded")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	//get id book from the request parameter, the key is "id"
	params := mux.Vars(r)

	// conversion of id from string to int
	id, err := strconv.Atoi(params["id"])

	if err != nil {
		log.Fatalf("Cannot change from string to int.  %v", err)
	}

	// calling models retrieves one book with id parameter which will later retrieve single data
	book, err := models.GetOneBook(int64(id))

	if err != nil {
		log.Fatalf("Can't retrieve book data. %v", err)
	}

	// Send the response
	json.NewEncoder(w).Encode(book)
}

// Get all book data
func GetAllBook(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Context-Type", "application/x-www-form-urlencoded")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	// calling models RetrieveAllBooks
	books, err := models.GetAllBook()

	if err != nil {
		log.Fatalf("Can't retrieve data. %v", err)
	}

	var response Response
	response.Status = 1
	response.Message = "Success"
	response.Data = books

	// Send all the response
	json.NewEncoder(w).Encode(response)
}

func UpdateBook(w http.ResponseWriter, r *http.Request) {

	// we take the request id parameter
	params := mux.Vars(r)

	// convert to int which was previously a string
	id, err := strconv.Atoi(params["id"])

	if err != nil {
		log.Fatalf("Can't change from string to int.  %v", err)
	}

	// create a book variable with type models. Book
	var book models.Book

	// decode json request to book variable 
	err = json.NewDecoder(r.Body).Decode(&book)

	if err != nil {
		log.Fatalf("Cannot decode request body.  %v", err)
	}

	// call update books to update data
	updatedRows := models.UpdateBook(int64(id), book)

	// this is the format of a message in the form of a string
	msg := fmt.Sprintf("The book has been successfully updated. Updated amount %v rows/record", updatedRows)

	// this is the format of the response message
	res := response{
		ID:      int64(id),
		Message: msg,
	}

	// send in the form of a response
	json.NewEncoder(w).Encode(res)
}

func DeleteBook(w http.ResponseWriter, r *http.Request) {

	// we take the request id parameter
	params := mux.Vars(r)

	//convert to int which was previously a string
	id, err := strconv.Atoi(params["id"])

	if err != nil {
		log.Fatalf("Can't change from string to int.  %v", err)
	}

	// call the delete a book function, and convert int to int64
	deletedRows := models.DeleteBook(int64(id))

	// this is the format of a message in the form of a string
	msg := fmt.Sprintf("The successful book was deleted. Total deleted data %v", deletedRows)

	// this is the response message format
	res := response{
		ID:      int64(id),
		Message: msg,
	}

	// send the response
	json.NewEncoder(w).Encode(res)
}