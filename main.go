package main

import (
	"fmt"
	router "golang-rest-api/routers"
	"log"
	"net/http"
)

func main() {
	r := router.Router()
	// fs := http.FileServer(http.Dir("build"))
	// http.Handle("/", fs)
	fmt.Println("Server running on port 8080!")

	log.Fatal(http.ListenAndServe(":8080", r))
}